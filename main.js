const Discord = require("discord.js");
const client = new Discord.Client();

client.on('message', message => {
  if (message.content.startsWith('xyz!avatar')) {
	  message.channel.sendMessage({
		  "embed": {
			  title: 'Avatar',
			  "image": {
				"url": message.author.avatarURL,
			  }
		  }
	  });
  }
});

client.login('token');

//use xyz!avatar and bot will send message author avatar
//in token place paste your bot token
